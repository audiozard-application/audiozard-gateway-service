import { type Config } from 'jest';

const config: Config = {
  moduleFileExtensions: new Array<string>('js', 'json', 'ts'),
  rootDir: 'src',
  testRegex: '.*\\.spec\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  moduleNameMapper: {
    '^@common/(.*)$': '<rootDir>/app/common/$1',
    '^@config/(.*)$': '<rootDir>/app/config/$1',
    '^@core/(.*)$': '<rootDir>/app/core/$1',
    '^@entities/(.*)$': '<rootDir>/app/entities/$1',
    '^@repositories/(.*)$': '<rootDir>/app/repositories/$1',
    '^@shared/(.*)$': '<rootDir>/app/shared/$1',
  },
  collectCoverageFrom: new Array<string>('**/*.(t|j)s'),
  coverageDirectory: '../.development/tests/coverage-unit',
  testEnvironment: 'node',
};

export default config;
