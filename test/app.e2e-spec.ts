import { type NestExpressApplication } from '@nestjs/platform-express';
import { Test, type TestingModule } from '@nestjs/testing';

import * as request from 'supertest';

import { AppModule } from './../src/app.module';

describe('AppController (e2e)', (): void => {
  let application: NestExpressApplication;

  beforeEach(async (): Promise<void> => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    application = moduleFixture.createNestApplication<NestExpressApplication>();
    await application.init();
  });

  it('/ (GET)', (): Test => {
    return request(application.getHttpServer()).get('/').expect(200).expect('Hello World!');
  });
});
