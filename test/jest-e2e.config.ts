import { type Config } from 'jest';

const config: Config = {
  moduleFileExtensions: new Array<string>('js', 'json', 'ts'),
  rootDir: '.',
  testRegex: '.e2e-spec.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  moduleNameMapper: {
    '^@common/(.*)$': '<rootDir>/../src/app/common/$1',
    '^@config/(.*)$': '<rootDir>/../src/app/config/$1',
    '^@core/(.*)$': '<rootDir>/../src/app/core/$1',
    '^@entities/(.*)$': '<rootDir>/../src/app/entities/$1',
    '^@repositories/(.*)$': '<rootDir>/../app/repositories/$1',
    '^@shared/(.*)$': '<rootDir>/../app/shared/$1',
  },
  collectCoverageFrom: new Array<string>('**/*.(t|j)s'),
  coverageDirectory: '../.development/tests/coverage-e2e',
  testEnvironment: 'node',
};

export default config;
